module.exports = (res, error) => {
  res.status(500).json({
     status: false,
     massage: error.massage ? error.message : error
  })
}