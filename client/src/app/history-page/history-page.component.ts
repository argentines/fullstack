import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MaterialInstance, MaterialService } from '../shared/classes/material.service';
import { OrdersService } from '../shared/services/orders.service';
import { Subscription } from 'rxjs';
import { Order, Filter } from '../shared/interfaces';

const STEP = 2;

@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.css']
})
export class HistoryPageComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('tooltipe') tooltipeRef: ElementRef;
  isFilterVisible = false;
  tooltipe: MaterialInstance;
  oSub: Subscription;
  orders: Order[] = [];
  loading = false;
  reloading = false;
  noMoreOrders = false;
  filter: Filter = {};

  offset = 0;
  limit = STEP;


  constructor(private orderService: OrdersService) { }

  ngOnInit() {
    this.reloading = true;
    this.fetch();
  }

  applyFilter(filter: Filter) {
    this.orders = [];
    this.offset = 0;
    this.filter = filter;
    this.reloading = true;
    this.fetch();
  }

  private fetch() {
    const params = Object.assign({}, this.filter, {
      offset: this.offset,
      limit: this.limit
    });

    this.oSub = this.orderService.fetch(params).subscribe((orders) => {
        this.orders = this.orders.concat(orders);
        this.noMoreOrders = orders.length < STEP;
        this.loading = false;
        this.reloading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.offset += STEP;
    this.fetch();
  }

  ngOnDestroy() {
    this.tooltipe.destroy();
    if (this.oSub) {
      this.oSub.unsubscribe();
    }
  }

  ngAfterViewInit() {
   this.tooltipe = MaterialService.initTooltip(this.tooltipeRef);
  }

  isFiltered(): boolean {
    return Object.keys(this.filter).length !== 0;
  }
}
