import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthLayoutsComponent } from './shared/layouts/auth-layouts/auth-layouts.component';
import { SiteLayoutsComponent } from './shared/layouts/site-layouts/site-layouts.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegistrPageComponent } from './registr-page/registr-page.component';
import { AuthGuard } from './shared/classes/auth.guard';
import { OvervewPageComponent } from './overvew-page/overvew-page.component';
import { AnalitycsPageComponent } from './analitycs-page/analitycs-page.component';
import { HistoryPageComponent } from './history-page/history-page.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { CategoriesPageComponent } from './categories-page/categories-page.component';
import { CategoriesFormComponent } from './categories-page/categories-form/categories-form.component';
import { OrderCategoriesComponent } from './order-page/order-categories/order-categories.component';
import { OrderPositionsComponent } from './order-page/order-positions/order-positions.component';

const routes: Routes = [
  {path: '', component: AuthLayoutsComponent, children: [
      {path: '', redirectTo: '/login', pathMatch: 'full'},
      {path: 'login', component: LoginPageComponent},
      {path: 'registr', component: RegistrPageComponent}
  ]},
  {path: '', component: SiteLayoutsComponent, canActivate: [AuthGuard], children: [
    {path: 'overvew', component: OvervewPageComponent},
    {path: 'analytics', component: AnalitycsPageComponent},
    {path: 'history', component: HistoryPageComponent},
    {path: 'order', component: OrderPageComponent, children: [
      {path: '', component: OrderCategoriesComponent},
      {path: ':id', component: OrderPositionsComponent}
    ]},
    {path: 'categories', component: CategoriesPageComponent},
    {path: 'categories/new', component: CategoriesFormComponent},
    {path: 'categories/:id', component: CategoriesFormComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
