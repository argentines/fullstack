import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from '../../shared/services/categories.service';
import { switchMap } from 'rxjs/operators';
import { MaterialService } from '../../shared/classes/material.service';
import { of } from 'rxjs';
import { Category } from '../../shared/interfaces';

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {

  @ViewChild('input') imputRef: ElementRef;
  isNew = true;
  form: FormGroup;
  imgePreview;
  image: File;
  category: Category;

  constructor(private route: ActivatedRoute,
              private categoriesService: CategoriesService,
              private router: Router) { }


  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required)
    });

    this.form.disable();
    this.route.params
     .pipe(
      switchMap((params: Params) => {
        if (params['id']) {
          this.isNew = false;
          return this.categoriesService.getById(params['id']);
        }
        return of(null);
      })
     ).subscribe(
        (category: Category) => {
          if (category) {
            this.category = category;
            this.form.patchValue({
              name: category.name
            });
            this.imgePreview = category.imageSrc;
            MaterialService.updateTextInput();
          }
          this.form.enable();
        },
        error => { MaterialService.toast(error.error.message); }
     );
  }

  triggerClick() {
    this.imputRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.image = file;

    const reader = new FileReader();
    reader.onload = () => {
      this.imgePreview = reader.result;
    };

    reader.readAsDataURL(file);
  }

  onSubmit() {
    let obs$;
    this.form.disable();
    if (this.isNew) {
      obs$ = this.categoriesService.create(this.form.value.name, this.image);
    } else {
      obs$ = this.categoriesService.update(this.category._id, this.form.value.name, this.image);
    }

    obs$.subscribe(
      (category) => {
        this.category = category;
        MaterialService.toast('Change is saved');
        this.form.enable();
      },
      (error) => {
        MaterialService.toast(error.error.message);
      }
    );
  }

  deleteCategory() {
    const desicion = window.confirm(`Are you sure you want delete category ${this.category.name}`);
    if (desicion) {
      this.categoriesService.delete(this.category._id)
          .subscribe(
            responce => MaterialService.toast(responce.message),
            error => MaterialService.toast(error.message),
            () => this.router.navigate(['/categories'])
          );
    }
  }

}
