import { Component, AfterViewInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { MaterialService, MaterialInstance } from '../../classes/material.service';

@Component({
  selector: 'app-site-layouts',
  templateUrl: './site-layouts.component.html',
  styleUrls: ['./site-layouts.component.css']
})
export class SiteLayoutsComponent implements AfterViewInit, OnDestroy {

  @ViewChild('floating')floatingRef: ElementRef;
  @ViewChild('menu')mobileMenuRef: ElementRef;

  links = [
    {url: '/overvew', name: 'Overview'},
    {url: '/analytics', name: 'Analytics'},
    {url: '/history', name: 'History'},
    {url: '/order', name: 'Add order'},
    {url: '/categories', name: 'Assortment'}
  ];

  menu: MaterialInstance;
  constructor(private auth: AuthService,
              private router: Router) { }


  ngAfterViewInit() {
    MaterialService.initiaLizeFloatingButton(this.floatingRef);
    this.menu = MaterialService.initMobileMenu(this.mobileMenuRef);
  }

  ngOnDestroy() {
    this.menu.destroy();
  }

  logout(event: Event) {
    event.preventDefault();
    this.auth.logout();
    this.router.navigate(['/login']);
  }

  openMmenu(event: Event) {
    event.preventDefault();
    this.menu.open();
  }
}
