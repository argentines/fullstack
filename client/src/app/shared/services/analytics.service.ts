import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OverViewPage, AnalitycsPage } from '../interfaces';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor(private http: HttpClient) {}

  getAnalitycs(): Observable<AnalitycsPage> {
    return this.http.get<AnalitycsPage>('/api/analytics/analytics');
  }

  getOvervew(): Observable<OverViewPage> {
    return this.http.get<OverViewPage>('/api/analytics/overvew');
  }
}
