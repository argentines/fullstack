export interface User {
  email: string;
  pwd: string;
}

export interface Message {
  message: string;
}

export interface Category {
  name: string;
  imageSrc?: string;
  user?: string;
  _id?: string;
}

export interface Position {
  name: string;
  cost: number;
  user?: string;
  category: string;
  _id?: string;
  quantity?: number;
}

export interface Order {
  data?: Date;
  order?: number;
  list: OrderPosition[];
  user?: string;
  _id?: string;
}

export interface OrderPosition {
  name: string;
  cost: number;
  quantity: number;
  _id?: string;
}

export interface Filter {
  start?: Date;
  end?: Date;
  order?: number;
}

export interface OverViewPage {
  gain: OverViewPageItem;
  orders: OverViewPageItem;
}

export interface OverViewPageItem {
  percent: number;
  compare: number;
  yestoday: number;
  isHigher: boolean;
}

export interface AnalitycsPage {
  averege: number;
  chart: AnalitycsChartItem[];
}

export interface AnalitycsChartItem {
  gain: number;
  order: number;
  label: string;
}
