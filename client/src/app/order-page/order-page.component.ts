import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MaterialInstance, MaterialService } from '../shared/classes/material.service';
import { OrederService } from './order.service';
import { OrderPosition, Order } from '../shared/interfaces';
import { OrdersService } from '../shared/services/orders.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css'],
  providers: [OrederService]
})
export class OrderPageComponent implements OnInit, OnDestroy, AfterViewInit {

  oSub: Subscription;
  isRoot: boolean;
  modal: MaterialInstance;
  pending = false;
  constructor(private router: Router,
              public order: OrederService,
              private ordersService: OrdersService) { }

  @ViewChild('modal') modalRef: ElementRef;

  ngOnInit() {
    this.isRoot = this.router.url === '/order';
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isRoot = this.router.url === '/order';
      }
    });
  }

  ngOnDestroy() {
    this.modal.destroy();
  }

  ngAfterViewInit() {
   this.modal = MaterialService.initModal(this.modalRef);
  }

  removePosition(orderPosition: OrderPosition) {
    this.order.remove(orderPosition);
  }

  onOpen() {
   this.modal.open();
   if (this.oSub) {
     this.oSub.unsubscribe();
   }
  }

  onCansel() {
    this.modal.close();
  }

  onConfirm() {
    this.pending = true;
    const order: Order = {
      list: this.order.list.map((item) => {
        delete item._id;
        return item;
      })
    };
    this.oSub = this.ordersService.create(order).subscribe(
      (newOrder) => {
         MaterialService.toast(`Order №${newOrder.order} has added.`);
         this.order.clear();
      },
      (error) => { MaterialService.toast(error.error.message); },
      () => {
        this.modal.close();
        this.pending = false;
      }
    );
  }
}
