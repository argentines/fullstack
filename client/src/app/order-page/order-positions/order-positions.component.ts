import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PositionService } from '../../shared/services/position.service';
import { Position } from '../../../app/shared/interfaces';
import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { OrederService } from '../order.service';
import { MaterialService } from '../../shared/classes/material.service';

@Component({
  selector: 'app-order-positions',
  templateUrl: './order-positions.component.html',
  styleUrls: ['./order-positions.component.css']
})
export class OrderPositionsComponent implements OnInit {

  positions$: Observable<Position[]>;

  constructor(private route: ActivatedRoute,
              private positionService: PositionService,
              private order: OrederService) { }

  ngOnInit() {
    this.positions$ = this.route.params
        .pipe(
          switchMap((params: Params) => {
            return this.positionService.fetch(params['id']);
          })
        ),
        map(
          (positions: Position[]) => {
            return positions.map(position => {
              return position.quantity = 1;
            });
          }
        );
  }

  addToOrder(position: Position) {
    MaterialService.toast(`Added: <strong>${position.name} - x${position.quantity}</strong>`);
    this.order.add(position);
  }
}
