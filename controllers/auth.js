const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const keys = require('../config/keys')
const User = require('../models/User')
const errorHendler = require('../utils/errorHendler')

module.exports.login = async function(req, res){

  const candidad = await User.findOne({email: req.body.email});

  if(candidad){
    const pwdResalt = bcrypt.compareSync(req.body.pwd, candidad.pwd)
    if(pwdResalt){
      const token = jwt.sign({
        email: candidad.email,
        userId: candidad._id
      }, keys.jwt, {expiresIn: 60 * 60});

      res.status(200).json({
        token: `Bearer ${token}`
      })
    }else{
      res.status(401).json('The password is wrong, try again.')
    }
  }else{   
      res.status(404).json({
        message:'This user email don`t exist.'
      });
   }
}

module.exports.registr = async function(req, res){
  
   const candidad = await User.findOne({email: req.body.email});
   if(candidad){
     res.status(409).json({
       message:'This email alredy exist, triy again with other email.'
     });
   }else{
      const salt = bcrypt.genSaltSync(10)
      const pwd = req.body.pwd
      const user = new User({
        email: req.body.email,
        pwd: bcrypt.hashSync(pwd, salt)
      })

      try{
        await user.save()
        res.status(201).json(user)
      }catch(e){
        errorHendler(res, e)
      }
   }
}