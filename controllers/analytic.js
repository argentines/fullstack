const moment = require('moment');
const Order = require('../models/Order');
const errorHendler = require('../utils/errorHendler')

module.exports.overvew = async function(req, res){
  try{
    const allOrders = await Order.find({user: req.user.id}).sort({date: 1}); //All orders from db
    const ordersMap = getOrdersMap(allOrders); //Orders array farmat {'12.04.2019': [order],[order]}
    const yestodayOrders = ordersMap[moment().add(-1, 'd').format('DD.MM.YYYY')] || []; //Total orders by yestoday
    const yestodayOrdersNumber = yestodayOrders.length; //Yestoday orders total length
    const totalOrdersNumber = allOrders.length;//Orders total
    const daysNumber = Object.keys(ordersMap).length;//Days total 
    const ordersPerDay = (totalOrdersNumber / daysNumber).toFixed(0);//Total orders per day
    const ordersPercent = (((yestodayOrdersNumber / ordersPerDay) - 1) * 100).toFixed(2);//percentage for number of orders
    const totalGain = canculatePrice(allOrders);//total revenues
    const gainPerDay = totalGain / daysNumber;//total revenue per day
    const yestodayGain = canculatePrice(yestodayOrders).toFixed(2);//yestoday total revenue
    const gainPercent = (((yestodayGain / gainPerDay) -1) * 100).toFixed(2);//percent of revenue;
    const compareGain = (yestodayGain - gainPerDay).toFixed(2);//
    const compareNumber = (yestodayOrdersNumber - ordersPerDay).toFixed(2);//

    res.status(200).json({
      gain: {
        percent: Math.abs(+gainPercent),
        compare: Math.abs(+compareGain),
        yestoday: +yestodayGain,
        isHigher: +gainPercent > 0
      },
      orders: {
        percent: Math.abs(+ordersPercent),
        compare: Math.abs(+compareNumber),
        yestoday: +yestodayOrdersNumber,
        isHigher: +ordersPercent > 0
      }
    })
  }catch(e){
    errorHendler(res, e);
  }
}

module.exports.analytic = async function(req, res){ 
  try{
    const allOrders = await Order.find({user: req.user.id}).sort({date: 1});//All orders from db
    const ordersMap = getOrdersMap(allOrders);//Orders array farmat {'12.04.2019': [order],[order]}
    const averege = +(canculatePrice(allOrders) / Object.keys(ordersMap).length).toFixed(2);

    const chart = Object.keys(ordersMap).map((label) => {
      const gain = canculatePrice(ordersMap[label]);
      const order = ordersMap[label].length;

      return{label, order, gain}
    });

    res.status(200).json({
      averege, chart
    })
  }catch(e){
    errorHendler(res, e);
  }
}

function getOrdersMap(orders = []){
  const daysOrder = {};
  orders.forEach((order) => {
    const date = moment(order.date).format('DD.MM.YYYY');

    //if is today
    if(date === moment().format('DD.MM.YYYY')){
      return;
    }

    if(!daysOrder[date]){
      daysOrder[date] = [];
    }
    daysOrder[date].push(order);
  })
  return daysOrder;
}

function canculatePrice(orders = []){
  return orders.reduce((total, order) => {
     const orderCost = order.list.reduce((orderTotal, item) => {
        return orderTotal += item.cost * item.quantity;
     },0);
     return total += orderCost;
  },0);
}