module.exports = {
  mongoUri: process.env.MONGO_URI,
  dataConnect: { 
                 useNewUrlParser: true,
                 useCreateIndex: true 
               },
  jwt: process.env.JWT
}